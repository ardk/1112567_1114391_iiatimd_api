<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    //
    public function create(Request $request){
      $post = new Post;
      $post->user_id = Auth::user()->id;
      $post->desc = $request->desc;

      if($request->photo != ''){
        $photo = time().'jpg';
        file_put_contents('storage/posts/'.$photo,base64_decode($request->photo));
        $post->photo = $photo;

      }

      $post->save();
      $post->user;
      return response()->json([
        'success' => true,
        'message' => 'posted',
        'post' => $post,
      ]);
    }

    public function update(Request $request){
      $post = Post::find($request->id);
      if(Auth::user()->id != $post->user_id){
        return response()->json([
          'success' => false,
          'message' => 'unauthorized access'
        ]);
      }
      $post->desc = $request->desc;
      $post->update();
      return response()->json([
        'success' => true,
        'message' => 'post edited'
      ]);
    }

    public function delete(Request $request){
      $post = Post::find($request->id);
      if(Auth::user()->id != $post->user_id){
        return response()->json([
          'success' => false,
          'message' => 'unauthorized access'
        ]);
      }

      if($post->photo != ''){
        Storage::delete('public/posts/'.$post->photo);
      }
      $post->delete();

      return response()->json([
        'success' => true,
        'message' => 'post deleted'
      ]);
    }

    public function posts(){
      $posts = Post::orderBy('id','desc')->get();
      foreach($posts as $post){
        $post->user;
        // count comments
        $post['commentsCount'] = count($post->comments);

      }

      return response()->json([
        'success' => true,
        'posts' => $posts
      ]);
    }

    public function myPosts() {
      $posts = Post::where('user_id', Auth::user()->id)->orderBy('id','desc')->get();
      $user = Auth::user();
      return response()->json([
        'success' => true,
        'posts' => $posts,
        'user' => $user
      ]);
    }



}
