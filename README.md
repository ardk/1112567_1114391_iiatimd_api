# IIATIMD
Deze API is gerealiseerd tijdens de module IIATIMD.

## Diensten Talent
Diensten Talent is een android applicatie waarbij freelancers hun diensten kunnen aanbieden in ruil voor andere diensten. Om dit concept te verduidelijken volgt hier een praktisch voorbeeld: Een webdesigner biedt een responsive website aan in ruil voor een nieuwe voortuin. Of een schilder die de kozijnen komt schilderen in ruil voor een nieuwe grasmat in zijn achtertuin.

Deze applicatie werkt in combinatie met de API die gemaakt is in Laravel. Alle authenticatie met betrekking tot de accounts gaat via JWT en alle data wordt opgeslagen in onze database.

## Informatie
De git flow die wij hanteren tijdens dit project is als volgt. Per User Story of nieuwe feature wordt een nieuwe branch gemaakt die afstamt van de Development branch. Vervolgens wordt er een pull request aangemaakt die door de teamgenoot gecheckt wordt middels een code review. Na een approve wordt deze gemerged op de development branch. Voor de oplevering zal de code op de master branch gemerged worden.

## Installatie

- Clone de repository
- Maak een database aan
```bash
$ mysql -u <username> -p
$ CREATE DATABASE <db name>;
$ exit
```

- Maak een .env file aan en vul DB gegevens in
```bash
$ cp .env.example .env
```

- Voer een aantal commando's uit om de development omgeving klaar te maken
```bash
$ composer install
$ php artisan key:generate
$ php artisan migrate(:fresh)
$ php artisan jwt:secret
$ php artisan storage:link
```

- Om de mappen structuur van de storage:link over te nemen, dien je de volgende commando's uit te voeren (vanaf root laravel folder!)
```bash
$ cd storage/app/public
$ mkdir profiles
$ mkdir posts
$ cd ..
$ cd ..
$ mkdir -pv framework/views app framework/sessions framework/cache
$ cd ..
$ chmod 777 -R storage
$ chown -R www-data:www-data storage
```
